/**
 * @file
 * JavaScript recommend tag update.
 */

(function ($, Drupal, drupalSettings) {
  'use strict';

  /**
   * Attaches the batch behavior for notification.
   *
   * @type {Drupal~behavior}
   */
  Drupal.behaviors.assistant = {
    attach: function (context, settings) {
      var elem = $('#assistant').once('run');
      if (elem.length) {
        $.each(settings.assistant_settings.trigger_fields, function(k,v){
          if ($('[name=' + v + '\\[0\\]\\[value\\]]').length > 0) {
            $('[name=' + v + '\\[0\\]\\[value\\]]').on('blur', function(e){
              getRecommend($(this).val());
            });
          }
        });
        var valid_instances = [];
        $.each(CKEDITOR.instances, function(key, instance) {
          $.each(settings.assistant_settings.trigger_fields, function(i, field){
            // Field in text area such as "edit-body-0-value".
            field = field.replace(/_/g, '-');
            var textarea_field = 'edit-' + field + '-0-value';
            if (textarea_field == key) {
              valid_instances.push(instance);
            }
          });
        });
        $.each(valid_instances, function(key, instance){
          instance.on('blur', function(e) {
           getRecommend(instance.getData());
          });
        });
      }
      $('#assistant-visual').once().on('click', function(e){
        $('.box1.bubble').toggle(1000);
      });
      $(document).once('term-name').on('click', '.term-field .term-name', function(){
        var elem_id = $(this).siblings('div').first().attr('id');
        var target_id = elem_id.substr(0, elem_id.length - '-term'.length);
        scrollToHighlight($('#' + target_id), 500);
      });

      // Handle click on add term -> set value.
      $(document).once('apply-term').on('click', 'a.apply-term', function(e) {
        e.preventDefault();
        var field_id = getFieldIdByName($(this).data('field'));
        var elem = $('#'+field_id);
        var tid =$(this).data('tid');
        if (elem[0].nodeName == 'SELECT') {
          if (elem[0].multiple === false){
            elem.val(tid);
          }
          else {
            // Multiple select.
            var val = elem.val();
            // Remove '_none' value.
            val = jQuery.grep(val, function(value) {
              return value != '_none';
            });
            val.push(tid);
            elem.val(val);
          }
          applyAnimation($(this), elem);
        }
        // Autocomplete tag.
        else if(elem[0].nodeName == 'INPUT') {
          var new_val = tagStyleInput(elem, $(this).parents('.term').text(), tid);
          elem.val(new_val);
          applyAnimation($(this), elem);
        }
      });

      // Ajax to get recommend tag.
      function getRecommend(input_str) {
        var bundle = settings.assistant_settings.bundle;
        $.ajax({
          url: Drupal.url('ttv_assistant/recommend/' + settings.assistant_settings.bundle + '?_format=json'),
          type: 'POST',
          headers: {
            'Content-Type': 'application/json'
          },
          data: JSON.stringify(input_str),
          dataType: 'json',
          async: false,
          success: function (response) {
            if (response.length) {
              parseResponse(response);
            }
            else{
              $('.block-ttv-assistant').fadeOut('slow');
            }
          },
          error: function (XMLHttpRequest, textStatus, errorThrown) {
            console.log(errorThrown);
          }
        });
      }

      // Parse response tags to assistant.
      function parseResponse(response) {
        var html = '';
        if ($('.block-ttv-assistant').is(':visible')) {
          $('#assistant h2').text(Drupal.t(settings.assistant_settings.message));
        }
        // Available tags for add new.
        $.each(settings.assistant_settings.taxonomy_fields, function(field_name, item) {
          var field_id = getFieldIdByName(field_name);
          var label = $('label[for=' + field_id +']').text();
          var content_prefix = '<div class="term-field">';
          content_prefix += '<h3 class="term-name">' + label + '</h3>';
          content_prefix += '<div id="'+ field_id  +'-term" class="term-content">';
          var content = '';
          $.each(response, function(k, term) {
            // Not add suggestion for already exists term.
            if (term.vid in item.target_bundles) {
              if (tagExist(field_id, term) === false) {
                content += printTerm(field_name, term);
              }
            }
          });
          var content_suffix = '</div></div>';
          if (content.length) {
            html += content_prefix + content + content_suffix;
          }
        });
        $('#term-result').html(html);
        if (html.length) {
          $('#term-result').show();
          $('#assistant h2').text(Drupal.t(settings.assistant_settings.message));
          $('.block-ttv-assistant').fadeIn('slow');
        }
        else{
          $('#term-result').hide();
        }
      }

      /**
       * Get field id by field name for futher use.
       */
      function tagExist(field_id, term) {
        var exists = false;
        var current_val = $('#' + field_id).val();
        if ($('#' + field_id)[0].nodeName == 'SELECT' &&
          (current_val == term.tid || $.inArray(term.tid, current_val) !== -1)) {
          return true;
        }
        else if($('#' + field_id)[0].nodeName == 'INPUT') {
          var tag = term.title + ' \\(' + term.tid + '\\)';
          var pattern = new RegExp('\\b' + tag + '\\B', 'g');
          if (current_val.match(pattern)) {
            return true;
          }
        }
        return exists;
      }

      function escapeRegex(string) {
        return string.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
      }

      /**
       * Get field id by field name for futher use.
       */
      function getFieldIdByName(field_name) {
        var field_id = $("[name='" + field_name + "']").attr('id');
        if (typeof field_id === "undefined") {
          field_id = $("[name='" + field_name + "\\[\\]']").attr('id');
          if (typeof field_id === "undefined") {
            // Multiple value.
            field_id = $("[name='" + field_name + "\\[0\\]\\[target_id\\]']").attr('id');
            // Field field_tags.
            if (typeof field_id == "undefined") {
              field_id = $("[name='" + field_name + "\\[target_id\\]']").attr('id');
            }
          }
        }
        return field_id;
      }

      // Render term element for assistant.
      function printTerm(field_name, term) {
        var html = '';
        html += '<span class="term">';
        html += term.title;
        html += '<a href="#" class="apply-term" data-field="'+ field_name +'" data-tid="'+ term.tid +'"></a>';
        html += '</span>';
        return html;
      }

      // Tag style : name (tid).
      function tagStyleInput(elem, name, tid) {
        var old_val = elem.val();
        var new_val = '';
        var tag_val
        if (old_val.length <= 0) {
          new_val = name + ' (' + tid + ') ';
        }
        else {
          var added_val = name + ' (' + tid + ')';
          var arr_val = old_val.split(',');
          if ($.inArray(added_val, arr_val) === -1 && $.inArray(' ' + added_val, arr_val) === -1) {
            new_val = old_val + ', ' + added_val;
          }
          else {
            new_val = old_val;
          }
        }
        return new_val;
      }
      function applyAnimation(this_obj, elem) {
        var term_elem = this_obj.parents('span.term');
          term_elem.hide('slow', function(){
          if (this_obj.parents('.term-field').find('span.term').length == 1) {
            this_obj.parents('.term-field').hide('slow', function() {
              if ($(this).parents('.bubble').find('.term-field:visible').length == 0) {
                $('#term-result').fadeOut('2000');
                $('#assistant h2').text(Drupal.t('No more available tag!'));
                $(this).remove();
                $('.block-ttv-assistant').fadeOut('3000');
              }
              this_obj.remove();
            });
          }
          term_elem.remove();
        });
        scrollToHighlight(elem, 500);
      }
      function scrollToHighlight(elem, duration) {
        $([document.documentElement, document.body]).animate({
          scrollTop: elem.offset().top - settings.assistant_config.fixed_top
        }, duration);
        elem.parents('.form-item').addClass('blink');
        setTimeout(
          function() {
            elem.parents('.form-item').removeClass('blink');
          }, 1000);
      }
    }
  };
})(jQuery, Drupal, drupalSettings);
