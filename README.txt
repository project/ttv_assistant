TTV Assistant

Description
--------------------------
TTV Assistant is a simple assistant to help you extract the tags from input text easilier.
It also allow you to set the value of the desired field by one click. 

Installation
--------------------------
1. Copy the entire ttv_assistant directory the Drupal modules/contrib
directory or use Drush with drush dl ttv_assistant.
2. Login as an administrator. Enable the module on the Modules page.

Usage
--------------------------
Access '/admin/config/system/ttv_assistant' to set your favorite assistant avatar, your message
limit recomment tags and search fields based on node bundle.
- Maximum recommend tags: -1 for unlimit
- Check 'Case sensitivity' to search term by case sensitivity. 
- Ignored vocabularies to reduce/avoid search on selected vocabularies.
- Some elements with position 'fixed' or display 'none' may cause jquery offset.top not correct.
set as your own to display it correctly. 