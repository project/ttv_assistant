<?php

namespace Drupal\ttv_assistant\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * Controller for assistant functions.
 */
class AssistantController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public static function getRecommend() {
    $assistantService = \Drupal::service('ttv_assistant.assistant');
    $bundle = \Drupal::routeMatch()->getParameter('bundle');
    $input = \Drupal::request()->getContent();
    $recommend = '';
    if (!empty($input)) {
      $input = trim(strip_tags(html_entity_decode($input)));
      $input = str_replace(['\n', '\r'], ' ', $input);
      $recommend = $assistantService::getRecommendTag($bundle, $input);
    }
    return new JsonResponse($recommend);
  }

}
