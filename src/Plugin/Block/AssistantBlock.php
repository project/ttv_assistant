<?php

namespace Drupal\ttv_assistant\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\Entity\File;
use Drupal\Core\Url;

/**
 * Provides a block with list of recommend tags via taxonomy.
 *
 * @Block(
 *   id = "assistant_block",
 *   admin_label = @Translation("Assistant block"),
 *   category = @Translation("TTV Block")
 * )
 */
class AssistantBlock extends BlockBase implements ContainerFactoryPluginInterface, BlockPluginInterface {

  /**
   * Drupal\Core\Session\AccountInterface definition.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
    $plugin_id,
    $plugin_definition,
    AccountInterface $current_user) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container,
    array $configuration,
    $plugin_id,
    $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('current_user')
    );
  }

  /**
   * Block config form.
   */
  public function blockForm($form, FormStateInterface $form_state) {
    $form = parent::blockForm($form, $form_state);
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function blockSubmit($form, FormStateInterface $form_state) {
    parent::blockSubmit($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function blockAccess(AccountInterface $account) {
    return AccessResult::allowedIf($account->isAuthenticated());
  }

  /**
   * {@inheritdoc}
   */
  public function build() {
    $route_name = \Drupal::routeMatch()->getRouteName();
    if (!in_array($route_name, ['node.add', 'entity.node.edit_form'])) {
      return;
    }
    $assistant_config = \Drupal::config('ttv_assistant.settings');
    $avatar = '';
    if (isset($assistant_config->get('avatar')['fids'])) {
      $file = File::load($assistant_config->get('avatar')['fids']);
      if ($file) {
        $avatar = file_create_url($file->getFileUri());
      }
    }
    return [
      '#theme' => 'assistant_widget',
      '#attached' => [
        'library' => [
          'ttv_assistant/assistant',
        ],
        'drupalSettings' => [
          'assistant_config' => $assistant_config->getRawData(),
        ],
      ],
      '#assistant_config' => [
        'avatar' => $avatar,
        'message' => $assistant_config->get('message') ?? 'New tag found!',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCacheMaxAge() {
    return 0;
  }

}
